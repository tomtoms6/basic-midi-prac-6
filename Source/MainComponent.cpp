/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    ADM.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    ADM.addMidiInputCallback(String::empty, this);
    midiLabel.setBounds(0, 0, getWidth(), getHeight()/10);
    midiLabel.setText("Label", dontSendNotification);
    addAndMakeVisible(midiLabel);
}

MainComponent::~MainComponent()
{
    ADM.removeMidiInputCallback(String::empty, this);
}

void MainComponent::resized()
{

}

void MainComponent::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    DBG("MidiMessage\n");
    
    String midiText;
    
    if(message.isNoteOnOrOff())
    {
        midiText << "NoteOn: Channel" << message.getChannel();
        midiText << ":Number" << message.getNoteNumber();
        midiText << ":Velocity" << message.getVelocity();
        
    }
    
    midiLabel.getTextValue() = midiText;
}

